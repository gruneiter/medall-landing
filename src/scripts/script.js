import Splide from '@splidejs/splide';
import sh from '../blocks/show-hide/show-hide';
import tabs from '../blocks/tabs/tabs';
import reasonHeight from '../blocks/reason/reason';
import galleryAdvantages from '../blocks/gallery-advantages/gallery-advantages';
import headerMobileSwitcher from '../blocks/header-mobile-switcher/header-mobile-switcher';
import sequence from '../blocks/sequence/sequence';

const slidersSimple = Array.from(document.querySelectorAll('.slider--simple'));
const slidersWorks = Array.from(document.querySelectorAll('.slider--works'));
const slidersFeedback = Array.from(document.querySelectorAll('.slider--feedback'));

slidersSimple.forEach((slider) => {
  new Splide(slider, {
    type: 'loop',
    arrows: false,
    autoplay: true,
    interval: 5000,
  }).mount();
});
slidersWorks.forEach((slider) => {
  new Splide(slider, {
    type: 'loop',
    autoWidth: true,
    focus: 'center',
  }).mount();
});
slidersFeedback.forEach((slider) => {
  new Splide(slider, {
    type: 'loop',
    // autoWidth: true,
  }).mount();
});

sh();
tabs();
reasonHeight();
headerMobileSwitcher();
galleryAdvantages();
sequence();
