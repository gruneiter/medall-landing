const setEndWidth = (end) => {
  const endWidth = document.documentElement.clientWidth - end.getBoundingClientRect().right;
  end.parentElement.style.setProperty('--end-width', `${endWidth}px`);
};

const galleryAdvantages = () => {
  const ga = document.querySelector('.gallery-advantages__item.grid__cell--l-2');
  const end = document.querySelector('.gallery-advantages__end');
  ga.parentElement.style.setProperty('--item-height', `${ga.offsetWidth}px`);
  setEndWidth(end);
  window.addEventListener('resize', () => {
    ga.parentElement.style.setProperty('--item-height', `${ga.offsetWidth}px`);
    setEndWidth(end);
  });
};

export default galleryAdvantages;
