const setHeight = (elem) => {
  const width = elem.offsetWidth;
  elem.style.setProperty('height', `${width}px`);
};

const rh = () => {
  const ri = Array.from(document.querySelectorAll('.reason__image, .reason__add'));
  ri.forEach((item) => {
    setHeight(item);
  });
  window.addEventListener('resize', () => {
    ri.forEach((item) => {
      setHeight(item);
    });
  });
};

export default rh;
