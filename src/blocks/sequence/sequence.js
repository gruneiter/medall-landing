const sequence = () => {
  const items = Array.from(document.querySelector('.sequence').children);
  items.forEach((item, i) => {
    if ((i + 1) % 6 === 4) item.style.setProperty('--order-l', i + 2);
    else if ((i + 1) % 6 === 0) item.style.setProperty('--order-l', i - 2);
    else item.style.setProperty('--order-l', i);
    if ((i + 1) % 4 === 3) item.style.setProperty('--order-m', i + 1);
    else if ((i + 1) % 4 === 0) item.style.setProperty('--order-m', i - 1);
    else item.style.setProperty('--order-m', i);
  });
};

export default sequence;
